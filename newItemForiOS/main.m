//
//  main.m
//  newItemForiOS
//
//  Created by 郭文魁 on 17/7/19.
//  Copyright © 2017年 郭文魁. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
