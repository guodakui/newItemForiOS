//
//  AppDelegate.h
//  newItemForiOS
//
//  Created by 郭文魁 on 17/7/19.
//  Copyright © 2017年 郭文魁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

